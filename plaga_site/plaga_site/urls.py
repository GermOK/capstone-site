"""plaga_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.views.generic import RedirectView
from django.contrib import admin
from django.urls import include, path
from accounts import views as account_views
from catalog import views as cat_views
from . import views

#Add Django site authentication urls (for login, logout, password management)
urlpatterns = [
    path('polls/', include('polls.urls')),
    path('accounts/', include('accounts.urls')),
    # path('', include('templates/admin/base_site.html')),
    path('', views.IndexView.as_view(), name='index'),
    # # path('', views.HomePage.as_view(), name="home"),
    # path('accounts_app/', include('accounts.urls')),
    # path('', RedirectView.as_view(url='/admin/base_site.html')),
    # path('catalog/', include('catalog.urls')),
    # # path('', RedirectView.as_view(url='/catalog/')),
    path('admin/', admin.site.urls),
    # path(r"^accounts/", include("django.contrib.auth.urls")),
    # path(r"^accounts/", include("accounts.urls", namespace="accounts")),
]
